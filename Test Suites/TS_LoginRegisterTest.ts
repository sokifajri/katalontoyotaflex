<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>TS_LoginRegisterTest</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2018-05-23T09:12:19</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>1e6695f7-9e94-4b37-9ed1-062e9bb1453f</testSuiteGuid>
   <testCaseLink>
      <guid>1ec910f5-1b67-4d45-a583-5ec9d06c7900</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TC001_Test Login Fail</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>90744282-7a9b-4b14-8b4a-ef01d10efdcf</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/Email Data</testDataId>
      </testDataLink>
   </testCaseLink>
   <testCaseLink>
      <guid>527709dd-d7c6-49aa-9631-50068e0931fc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC002_Test Login Success</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
