import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

//Mobile.waitForElementPresent(findTestObject('Calculator Page/txtSimulasiBaru'), 10)
//Mobile.scrollToText('JAKARTA-PONDOK INDAH', FailureHandling.STOP_ON_FAILURE)
//'Get Device Height and Store in to device_height variable'
//device_Height = Mobile.getDeviceHeight()
//
//'Get Width Height and Store in to device_Width variable'
//device_Width = Mobile.getDeviceWidth()
//
//'Storing the startX value by dividing device width by 2. Because x coordinates are constant for Vertical Swiping'
//int startX = device_Width / 2
//
//'Here startX and endX values are equal for vertical Swiping for that assigning startX value to endX'
//int endX = startX
//
//'Storing the value in startY variable'
//int startY = device_Height * 0.30
//
//'Storing the value in endY variable'
//int endY = device_Height * 0.70
//
//'Swipe Vertical from bottom to top'
//Mobile.swipe(startX, startY, endX, endY)
//
//Mobile.delay(30, FailureHandling.STOP_ON_FAILURE)
//Mobile.scrollToText('JAKARTA-MANGGA DUA', FailureHandling.STOP_ON_FAILURE)
//
//Mobile.scrollToText('JAKARTA-KELAPA GADING', FailureHandling.STOP_ON_FAILURE)
//
//Mobile.scrollToText('DURI', FailureHandling.STOP_ON_FAILURE)
//
//Mobile.scrollToText('DEPOK', FailureHandling.STOP_ON_FAILURE)
//
//Mobile.scrollToText('DENPASAR', FailureHandling.STOP_ON_FAILURE)
//
//Mobile.scrollToText('CIREBON', FailureHandling.STOP_ON_FAILURE)
//
//Mobile.scrollToText('BSD CITY', FailureHandling.STOP_ON_FAILURE)
//
//Mobile.scrollToText('BOGOR', FailureHandling.STOP_ON_FAILURE)
Mobile.tap(findTestObject('Calculator Page/btnOkCity'), 0)

Mobile.tap(findTestObject('Calculator Page/btnOKCar'), 0)

Mobile.tap(findTestObject('Calculator Page/btnOkOTR'), 0)

Mobile.setText(findTestObject('Calculator Page/edtTDP'), '100000000', 0)

Mobile.tap(findTestObject('Calculator Page/btnOkTDPInstallment'), 0)

Mobile.tap(findTestObject('Calculator Page/rbtnTenor36'), 0)

Mobile.tap(findTestObject('Calculator Page/btnOkTenor'), 0)

Mobile.tap(findTestObject('Calculator Page/btnOkInsurance'), 0)

Mobile.tap(findTestObject('Calculator Page/btnOKTACP'), 0)

Mobile.tap(findTestObject('Calculator Page/btnHitungSimulasi'), 0)

Mobile.waitForElementPresent(findTestObject('Result Calcultor Page/txtPilihHasilSimulasi'), 60)

Mobile.verifyElementAttributeValue(findTestObject('Result Calcultor Page/txtPilihHasilSimulasi'), 'text', 'Pilih Hasil Simulasi Anda', 
    0)

