import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

Mobile.setText(findTestObject('LoginRegister Page/edtEmailLogin'), GlobalVariable.srtEmailExisting, 10)

Mobile.hideKeyboard()

txtEmail = Mobile.getAttribute(findTestObject('LoginRegister Page/edtEmailLogin'), 'text', 10)

Mobile.verifyNotEqual(txtEmail, null)

Mobile.verifyMatch(txtEmail, GlobalVariable.patternEmail, true)

Mobile.tap(findTestObject('LoginRegister Page/btnMasuk'), 10)

Mobile.waitForElementPresent(findTestObject('LoginRegister Page/txtNoteVerification'), 10)

Mobile.setText(findTestObject('LoginRegister Page/edtToken'), GlobalVariable.strToken, 2)

Mobile.hideKeyboard()

Mobile.tap(findTestObject('LoginRegister Page/btnOK'), 0)

Mobile.waitForElementPresent(findTestObject('Setting Fragment/btnLogout'), 30)

not_run: txtLogout = Mobile.getAttribute(findTestObject('Setting Fragment/txtLogout'), 'text', 0)

not_run: Mobile.verifyEqual('txtLogout', 'Logout')

